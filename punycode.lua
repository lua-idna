-- Lua implementation of Punycode (RFC 3492)
-- Copyright (C) 2021 Sergey Poznyakoff
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 3, or (at your option)
-- any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

local punycode = {}

constant = require 'constant'

local BOOTSTRING = require 'constant' {
   base         = 36,
   tmin         = 1,
   tmax         = 26,
   skew         = 38,
   damp         = 700,
   initial_bias = 72,
   initial_n    = 128
}

local function adapt (delta,numpoints,firsttime)
   if firsttime then
      delta = delta // BOOTSTRING.damp
   else
      delta = delta // 2
   end
   delta = delta + delta // numpoints
   local k = 0
   while delta > ((BOOTSTRING.base - BOOTSTRING.tmin) * BOOTSTRING.tmax) // 2 do
      delta = delta // (BOOTSTRING.base - BOOTSTRING.tmin)
      k = k + BOOTSTRING.base
   end
   return k + (BOOTSTRING.base - BOOTSTRING.tmin + 1) * delta // (delta + BOOTSTRING.skew)
end

-- 0..25 map to ASCII a..z or A..Z 
-- 26..35 map to ASCII 0..9        

local function encode_digit (d)
   if d < 26 then
      d = d + 75
   end
   return utf8.char(d + 22)
end

local function decode_codepoint (d)
   if d >= 48 and d <= 57 then
      return d - 48 + 26
   elseif d >= 97 and d <= 122 then
      return d - 97
   else
      error(string.format("invalid codepoint %d", d))
   end
end

function punycode.encode (input)
   local n = BOOTSTRING.initial_n
   local delta = 0
   local bias = BOOTSTRING.initial_bias
   local input_len = utf8.len(input)

   local output = {}
   local codepoints = {}
   
   -- Copy basic codepoints to the output in order
   for _, c in utf8.codes(input) do
      if c < BOOTSTRING.initial_n then
	 table.insert(output, utf8.char(c))
      else
	 table.insert(codepoints, c)
      end
   end

   local h = #output
   if h > 0 then
      table.insert(output, '-')
   end

   local firsttime = true
   while h < input_len do
      -- Find the minimum code point >= n
      local m
      for j = 1, #codepoints do
	 if codepoints[j] >= n and ((not m) or codepoints[j] < m) then
	    m = codepoints[j]
	 end
      end
      -- print(string.format("next code point to insert is %x",m))

      delta = delta + (m - n) * (h + 1)
      if delta > math.maxinteger then
	 error(string.format("overflow on U+%d", m))
      end
      -- print(string.format("needed delta is %d", delta))
      
      n = m
      for _, c in utf8.codes(input) do
	 if c < n then
	    delta = delta + 1
	    if delta > math.maxinteger then
	       error(string.format("overflow on U+%d", m))
            end
	 elseif c == n then
	    local q = delta
	    local k = BOOTSTRING.base
	    while true do
	       local t
	       if k <= bias then
		  t = BOOTSTRING.tmin
	       elseif k >= bias + BOOTSTRING.tmax then
		  t = BOOTSTRING.tmax
	       else
		  t = k - bias
	       end

	       if q < t then break end

	       table.insert(output, encode_digit(t + ((q - t) % (BOOTSTRING.base - t))))
	       q = (q - t) // (BOOTSTRING.base - t)
	       k = k + BOOTSTRING.base
	    end

	    table.insert(output, encode_digit(q))
	    
	    bias = adapt(delta, h + 1, firsttime)
	    -- print(string.format("bias becomes %d", bias))
	    firsttime = false
	    
	    delta = 0
	    h = h + 1
	 end
      end
      -- print(string.format("delta now is %d", delta))
      delta = delta + 1
      n = n + 1
   end

   return table.concat(output)
end

function punycode.decode (input)
   local cp = {}
   local output = {}
   local delim
   
   -- Convert input to array of code points
   for i, c in utf8.codes(input) do
      if c >= BOOTSTRING.initial_n then
	 error("invalid input")
      else
	 cp[i] = c
	 if c == 45 then
	    delim = i
	 end
      end
   end

   local j
   if delim then
      -- consume all code points before the last delimiter
      for i = 1, delim-1 do
	 output[i] = utf8.char(cp[i])
      end
      -- start from the character past the last delimiter
      j = delim + 1
   else
      -- start from the first character
      j = 1
   end
     
   local n = BOOTSTRING.initial_n
   local bias = BOOTSTRING.initial_bias

   local i = 0
   while j <= #cp do
      local oldi = i
      local w = 1
      local k = BOOTSTRING.base
      while true do
	 if j > #cp then
	    error("unexpected end of input")
	 end
	 
	 local digit = decode_codepoint(cp[j])
	 j = j + 1

	 i = i + digit * w
	 
	 if i > math.maxinteger then
	    error(string.format("overflow in position %d", j))
         end

	 local t
	 if k <= bias then
	    t = BOOTSTRING.tmin
	 elseif k >= bias + BOOTSTRING.tmax then
	    t = BOOTSTRING.tmax
	 else
	    t = k - bias
	 end

	 if digit < t then break end

	 w = w * (BOOTSTRING.base - t)
	 if w > math.maxinteger then
	    error(string.format("overflow in position %d", j))
         end
	 k = k + BOOTSTRING.base
      end
      bias = adapt(i - oldi, #output + 1, oldi == 0)
      n = n + i // (#output + 1)
      i = i % (#output + 1)
      table.insert(output, i + 1, utf8.char(n))
      i = i + 1
   end
   return table.concat(output)
end

return punycode
