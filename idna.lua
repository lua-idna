-- Simplified Lua implementation of IDNA (RFC 3490)
-- Copyright (C) 2021 Sergey Poznyakoff
--
-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; either version 3, or (at your option)
-- any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

local idna = {}

punycode = require 'punycode'

-- Return an iterator function that, upon each call, returns the next
-- label from the name, and a boolean flag that is true if the label
-- consists entirely of the characters from the ASCII character set.
local function next_label (name)
   local f, s, v = utf8.codes(name)
   return function ()
      local label = {}
      local is_ascii = true
      while true do
	 local i,c = f(s, v)
         if i == nil then break end
	 v = i
	 if c == 0x2e or c == 0x3002 or c == 0xff0e or c == 0xff61  then
	    break
	 end
         if c > 127 then
	    is_ascii = false
	 end
	 table.insert(label, utf8.char(c))
      end
      if #label > 0 then
	 return table.concat(label), is_ascii
      end
   end
end

-- Return true if the label fulfills the STD3 ASCII requirements, i.e.
-- it does not begin or end with a '-' character and does not contain
-- forbidden ASCII characters.
--
-- See RFC 3490, Page 10, 3.
local function is_std3_ascii (label)
   if label:match('^-') or label:match('-$') then
      return false
   end
   for _,c in utf8.codes(label) do
      if (0 <= c and c <= 0x2C)
         or (0x2E <= c and c <= 0x2F)
         or (0x3A <= c and c <= 0x40)
	 or (0x5B <= c and c <= 0x60)
	 or (0x7B <= c and c <= 0x7F) then
	 return false
      end
   end
   return true
end

-- Implementation of the ToASCII function.
--
-- See RFC 3490, 4.1 ToASCII (pages 9 - 10)
--
-- NOTE: The implementation in its current form is simplified, as it
-- lacks the the NAMEPREP support.
--
function idna.ToASCII (name, is_ascii)
   if not is_ascii then
      -- FIXME: Perform NAMEPREP
   end
   if not is_std3_ascii(name) then
      return nil
   end
   if not is_ascii then
      if name:match('^xn--') then
	 return nil
      end
      local status, enc = pcall(punycode.encode, name)
      if status then
	 name = 'xn--' .. enc
      else
	 return nil
      end
   end

   if name:len() == 0 or name:len() > 63 then
      return nil
   end
   return name
end

-- Convert domain name to IDNA form (RFC 3490, 4. "Conversion operations")
-- On success, returns the converted domain name and nil.
-- On error, returns nil and the label from name that triggered the
-- error.
function idna.domain_name_to_ascii (name)
   local res = {}
   for label, is_ascii in next_label(name) do
      local ascii = idna.ToASCII(label, is_ascii)
      if ascii == nil then
	 return nil, label
      end
      table.insert(res, ascii)
   end
   return table.concat(res, '.'), nil
end

return idna

