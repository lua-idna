local constant = {}

function constant.declare(t)
   return setmetatable({}, {
	 __index = function (table, key)
	    if t[key] then
	       return t[key]
	    else
	       error("No such constant: "..key)
	    end
	 end;
	 __newindex = function(table, key, value)
		        error("Attempt to modify read-only table")
	 end;
	 __metatable = false;
   });
end

setmetatable(constant, {
		__call = function(_, ...) return constant.declare(...) end
})

return constant
