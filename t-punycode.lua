punycode = require 'punycode'

tests = {
   A = 'egbpdaj6bu4bxfgehfvwxn';
   B = 'ihqwcrb4cv8a8dqg056pqjye';
   C = 'ihqwctvzc91f659drss3x8bo0yb';
   D = 'Proprostnemluvesky-uyb24dma41a';
   E = '4dbcagdahymbxekheh6e0a7fei0b';
   F = 'i1baa7eci9glrd9b2ae1bj0hfcgg6iyaf8o0a1dig0cd';
   G = 'n8jok5ay5dzabd5bym9f0cm5685rrjetr6pdxa';
   H = '989aomsvi5e83db1d2a355cv1e0vak1dwrv93d5xbh15a0dt30a5jpsd879ccm6fea98c';
   I = 'b1abfaaepdrnnbgefbadotcwatmq2g4l';
   J = 'PorqunopuedensimplementehablarenEspaol-fmd56a';
   K = 'TisaohkhngthchnitingVit-kjcr8268qyxafd2f1b9g';
   L = '3B-ww4c5e180e575a65lsy2b';
   M = '-with-SUPER-MONKEYS-pc58ag80a8qai00g7n9n';
   N = 'Hello-Another-Way--fc4qua05auwb3674vfr0b';
   O = '2-u9tlzr9756bt3uc0v';
   P = 'MajiKoi5-783gue6qz075azm5e';
   Q = 'de-jg4avhby1noc0d';
   R = 'd9juau41awczczp'
}

tl = "ABCDEFGHIJKLMNOPQR"
for _,c in utf8.codes(tl) do
   local encoded = tests[utf8.char(c)]
   local decoded = punycode.decode(encoded);
   if punycode.encode(decoded) ~= encoded then
      print(string.format("%s FAILED", utf8.char(c)))
      print(encoded)
      print(punycode.encode(decoded))
   else
      print(string.format("%s OK", utf8.char(c)))
   end
end

      
	    

      
      

      
